package com.example.productdetailanimation

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

class MainActivity1 : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main_1)
    val activity = this

    val xyz = findViewById<View>(R.id.xyz)

    val llBottomSheet = findViewById<LinearLayout>(R.id.bottom_sheet)
    val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)

    val bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet)

    bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
      override fun onStateChanged(bottomSheet: View, newState: Int) {
        if (newState == BottomSheetBehavior.STATE_EXPANDED) {
          bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        } else if (newState == BottomSheetBehavior.STATE_HIDDEN) {
          bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
      }

      override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    })

    xyz?.setOnTouchListener(object : GestureHelper(activity) {
      override fun onSwipeTop() {
        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
          bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
      }

      override fun onSwipeBottom() {
        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_HIDDEN) {
          bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
      }

      override fun onClick() {
        Toast.makeText(activity, R.string.good, Toast.LENGTH_LONG).show()
      }

    })
    recyclerView.apply {
      // set a LinearLayoutManager to handle Android
      // RecyclerView behavior
      layoutManager = LinearLayoutManager(context)
      adapter = ListAdapter(mNicolasCageMovies)
    }
  }
}

private val mNicolasCageMovies = listOf(
  Movie("Raising Arizona", 1987),
  Movie("Vampire's Kiss", 1988),
  Movie("Con Air", 1997),
  Movie("Gone in 60 Seconds", 1997),
  Movie("National Treasure", 2004),
  Movie("The Wicker Man", 2006),
  Movie("Ghost Rider", 2007),
  Movie("Knowing", 2009),
  Movie("Raising Arizona", 1987),
  Movie("Vampire's Kiss", 1988),
  Movie("Con Air", 1997),
  Movie("Gone in 60 Seconds", 1997),
  Movie("National Treasure", 2004),
  Movie("The Wicker Man", 2006),
  Movie("Ghost Rider", 2007),
  Movie("Knowing", 2009),
  Movie("Raising Arizona", 1987),
  Movie("Vampire's Kiss", 1988),
  Movie("Con Air", 1997),
  Movie("Gone in 60 Seconds", 1997),
  Movie("National Treasure", 2004),
  Movie("The Wicker Man", 2006),
  Movie("Ghost Rider", 2007),
  Movie("Knowing", 2009),
  Movie("Raising Arizona", 1987),
  Movie("Vampire's Kiss", 1988),
  Movie("Con Air", 1997),
  Movie("Gone in 60 Seconds", 1997),
  Movie("National Treasure", 2004),
  Movie("The Wicker Man", 2006),
  Movie("Ghost Rider", 2007),
  Movie("Knowing", 2009),
  Movie("Raising Arizona", 1987),
  Movie("Vampire's Kiss", 1988),
  Movie("Con Air", 1997),
  Movie("Gone in 60 Seconds", 1997),
  Movie("National Treasure", 2004),
  Movie("The Wicker Man", 2006),
  Movie("Ghost Rider", 2007),
  Movie("Knowing", 2009),
  Movie("Raising Arizona", 1987),
  Movie("Vampire's Kiss", 1988),
  Movie("Con Air", 1997),
  Movie("Gone in 60 Seconds", 1997),
  Movie("National Treasure", 2004),
  Movie("The Wicker Man", 2006),
  Movie("Ghost Rider", 2007),
  Movie("Knowing", 2009),
  Movie("Raising Arizona", 1987),
  Movie("Vampire's Kiss", 1988),
  Movie("Con Air", 1997),
  Movie("Gone in 60 Seconds", 1997),
  Movie("National Treasure", 2004),
  Movie("The Wicker Man", 2006),
  Movie("Ghost Rider", 2007),
  Movie("Knowing", 2009),
  Movie("Raising Arizona", 1987),
  Movie("Vampire's Kiss", 1988),
  Movie("Con Air", 1997),
  Movie("Gone in 60 Seconds", 1997),
  Movie("National Treasure", 2004),
  Movie("The Wicker Man", 2006),
  Movie("Ghost Rider", 2007),
  Movie("Knowing", 2009)
)

data class Movie(val title: String, val year: Int)

class MovieViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
  RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item, parent, false)) {
  private var mTitleView: TextView? = null
  private var mYearView: TextView? = null


  init {
    mTitleView = itemView.findViewById(R.id.list_title)
    mYearView = itemView.findViewById(R.id.list_description)
  }

  fun bind(movie: Movie) {
    mTitleView?.text = movie.title
    mYearView?.text = movie.year.toString()
  }
}

class ListAdapter(private val list: List<Movie>) : RecyclerView.Adapter<MovieViewHolder>() {
  private val lastPosition = -1

  override fun onViewDetachedFromWindow(holder: MovieViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.itemView.clearAnimation()
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
    val inflater = LayoutInflater.from(parent.context)
    return MovieViewHolder(inflater, parent)
  }

  private fun setScaleAnimation(view: View) {
    val anim = ScaleAnimation(
      0.0f,
      1.0f,
      1.0f,
      1.0f,
      Animation.RELATIVE_TO_SELF,
      0.5f,
      Animation.RELATIVE_TO_SELF,
      0.5f
    )
    anim.duration = 400
    view.startAnimation(anim)
    /*if (position > lastPosition) {
  setScaleAnimation(holder.itemView)
  }*/
  }


  override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
    val movie: Movie = list[position]
    holder.bind(movie)
  }

  override fun getItemCount(): Int = list.size
}
